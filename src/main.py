import os
from argparse import ArgumentParser

import torch
import yaml

torch.set_float32_matmul_precision("high")  # Using tensor-cores

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import WandbLogger
from torchvision.transforms import Compose, Normalize, ToTensor

from src.data import CDPDataset
from src.model import PrinterModel


def main(config):
    # Unpacking parameters
    t_dir, x_dir, f_dir = config["t_dir"], config["x_dir"], config["f_dir"]
    train_p, val_p = config["train_p"], config["val_p"]
    batch_size = config["batch_size"]
    max_epochs = config["max_epochs"]
    exp_name = x_dir.split("/")[-1]

    # Dataset
    transform = Compose([ToTensor(), Normalize(255 / 2, 255 / 2)])

    dataset = CDPDataset(
        t_dir,
        x_dir,
        f_dir,
        train_p,
        val_p,
        batch_size,
        transform=transform,
        num_workers=0,  # os.cpu_count() would be best but can cause issues
    )

    # Model
    model = PrinterModel()

    # Training
    logger = WandbLogger(project="cdp", name=exp_name)
    logger.log_hyperparams(config)
    callbacks = [ModelCheckpoint("checkpoints/", exp_name, monitor="val_loss")]
    trainer = pl.Trainer(
        max_epochs=max_epochs,
        devices="auto",
        logger=logger,
        callbacks=callbacks,
    )
    trainer.fit(model, dataset)

    # Testing
    model = PrinterModel.load_from_checkpoint(f"checkpoints/{exp_name}.ckpt")
    trainer.test(model, dataset)


if __name__ == "__main__":
    # Getting configuration
    parser = ArgumentParser()
    parser.add_argument(
        "--config", type=str, help="Path to yaml configuration file", required=True
    )
    config_path = vars(parser.parse_args())["config"]

    config_file = open(config_path)
    config = yaml.safe_load(config_file)
    config_file.close()

    # Main program
    main(config)
