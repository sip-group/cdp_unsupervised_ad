import os
from argparse import ArgumentParser

import yaml


def main(args):
    # Unpacking args
    dataset_root = args["dataset_root"]
    max_epochs = args["epochs"]
    batch_size = args["batch_size"]
    train_p = args["train_p"]
    val_p = args["val_p"]
    config_dir = args["config_dir"]

    os.makedirs(config_dir, exist_ok=True)

    # Template, originals and fakes directories
    template_dir = os.path.join(dataset_root, "orig_template")
    orig_dir = os.path.join(
        dataset_root,
        "orig_phone_tifs",
        "HPI55_printdpi812.8_printrun1_session2_InvercoteG",
    )
    fake_dir = os.path.join(
        dataset_root,
        "fake_phone_tifs",
        "HPI55_printdpi812.8_printrun1_session2_InvercoteG_EHPI76",
    )

    for exp_name in os.listdir(orig_dir):
        if not os.path.isdir(os.path.join(fake_dir, exp_name)):
            continue

        conf = {
            "t_dir": template_dir,
            "x_dir": os.path.join(dataset_root, orig_dir, exp_name),
            "f_dir": os.path.join(dataset_root, fake_dir, exp_name),
            "train_p": train_p,
            "val_p": val_p,
            "batch_size": batch_size,
            "max_epochs": max_epochs,
        }

        with open(os.path.join(config_dir, exp_name + ".yaml"), "w") as f:
            yaml.dump(conf, f)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--dataset_root", type=str, required=True)
    parser.add_argument("--epochs", type=int, default=500)
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--train_p", type=float, default=0.6)
    parser.add_argument("--val_p", type=float, default=0.2)
    parser.add_argument("--config_dir", type=str, default="configs")
    args = vars(parser.parse_args())

    main(args)
