import pytorch_lightning as pl
import torch
import torch.nn as nn
from sklearn.metrics import auc, roc_curve
from torch.optim import Adam


class ShallowUNet(nn.Module):
    def __init__(self):
        super(ShallowUNet, self).__init__()

        self.down1 = nn.Sequential(
            nn.InstanceNorm2d(1, affine=True, track_running_stats=True),
            nn.Conv2d(1, 10, kernel_size=7, stride=1, padding=3, bias=False),
            nn.ReLU(inplace=True),
        )
        self.down2 = nn.Sequential(
            nn.InstanceNorm2d(10, affine=True, track_running_stats=True),
            nn.Conv2d(10, 20, kernel_size=4, stride=2, padding=1, bias=False),
            nn.ReLU(inplace=True),
        )
        self.down3 = nn.Sequential(
            nn.InstanceNorm2d(20, affine=True, track_running_stats=True),
            nn.Conv2d(20, 30, kernel_size=4, stride=2, padding=1, bias=False),
            nn.ReLU(inplace=True),
        )

        self.middle = nn.Sequential(
            nn.InstanceNorm2d(30, affine=True, track_running_stats=True),
            nn.Conv2d(30, 30, kernel_size=3, stride=1, padding=1, bias=False),
            nn.ReLU(inplace=True),
            nn.Conv2d(30, 30, kernel_size=3, stride=1, padding=1, bias=False),
            nn.ReLU(inplace=True),
        )

        self.up3 = nn.Sequential(
            nn.InstanceNorm2d(60, affine=True, track_running_stats=True),
            nn.ConvTranspose2d(60, 20, kernel_size=4, stride=2, padding=1, bias=False),
            nn.ReLU(inplace=True),
        )

        self.up2 = nn.Sequential(
            nn.InstanceNorm2d(40, affine=True, track_running_stats=True),
            nn.ConvTranspose2d(40, 10, kernel_size=4, stride=2, padding=1, bias=False),
            nn.ReLU(inplace=True),
        )

        self.up1 = nn.Sequential(
            nn.InstanceNorm2d(20, affine=True, track_running_stats=True),
            nn.ConvTranspose2d(20, 1, kernel_size=7, stride=1, padding=3, bias=False),
            nn.ReLU(inplace=True),
        )

        self.out = nn.Sequential(
            nn.InstanceNorm2d(1, affine=True, track_running_stats=True),
            nn.Conv2d(1, 1, kernel_size=1, stride=1, padding=0, bias=False),
            nn.Sigmoid(),
        )

    def forward(self, x):
        h1 = self.down1(x)
        h2 = self.down2(h1)
        h3 = self.down3(h2)
        h4 = self.middle(h3) + h3
        h5 = self.up3(torch.cat([h4, h3], dim=1))
        h6 = self.up2(torch.cat([h5, h2], dim=1))
        h7 = self.up1(torch.cat([h6, h1], dim=1))
        return self.out(h7)


class PrinterModel(pl.LightningModule):
    def __init__(self, lr=0.01):
        super().__init__()
        self.lr = lr
        self.original_scores_test = []
        self.fake_scores_test = []
        self.net = ShallowUNet()

    def configure_optimizers(self):
        optim = Adam(self.trainer.model.parameters(), lr=self.lr)
        return optim

    def forward(self, x):
        return self.net(x)

    def get_loss(self, batch):
        # t and y_hat have shape (B, C, H, W)
        # y has shape (B, P, C, H, W) where P is the max number of printed
        # n has shape (B,), and each element is the number of printed images to consider
        t = batch["template"]
        y_hat = self(t)
        y = batch["originals"]
        n = batch["n_originals"]

        # Computing the loss as the MSE between the reconstructed image and the original
        # Masking to zero the loss for the padded images
        mses = (y.transpose(0, 1) - y_hat).pow(2).mean(dim=(2, 3, 4))
        mask = torch.ones_like(mses)
        for i, ni in enumerate(n):
            mask[ni:, i] = 0

        loss = (mses * mask).mean()

        return loss

    def training_step(self, batch, batch_idx):
        loss = self.get_loss(batch)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        loss = self.get_loss(batch)
        self.log("val_loss", loss)
        return loss

    def test_step(self, batch, batch_idx):
        loss = self.get_loss(batch)
        self.log("test_loss", loss)

        t, x, f = batch["template"], batch["originals"], batch["fakes"]
        nx, nf = batch["n_originals"], batch["n_fakes"]

        y_hat = self(t)

        for i, nxi in enumerate(nx):
            mses_origs = ((y_hat[i] - x[i, :nxi]) ** 2).mean(dim=(1, 2, 3)).flatten()

        for i, nfi in enumerate(nf):
            mses_fakes = ((y_hat[i] - f[i, :nfi]) ** 2).mean(dim=(1, 2, 3)).flatten()

        self.original_scores_test.extend(mses_origs.cpu().numpy())
        self.fake_scores_test.extend(mses_fakes.cpu().numpy())
        self.log("test_loss", loss)
        return loss

    def on_test_epoch_end(self):
        # Computing the roc auc score
        fpr, tpr, _ = roc_curve(
            [0] * len(self.original_scores_test) + [1] * len(self.fake_scores_test),
            self.original_scores_test + self.fake_scores_test,
        )

        roc_auc = auc(fpr, tpr)
        self.log("roc_auc", roc_auc)
