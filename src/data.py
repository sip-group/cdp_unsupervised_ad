import os
import random

import cv2
import numpy as np
import torch
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, Dataset


class CDPSubset(Dataset):
    def __init__(
        self,
        template_ids,
        template_dir,
        original_dir,
        fake_dir,
        transform=None,
        max_printed=None,
    ):
        super(CDPSubset, self).__init__()

        self.template_ids = template_ids

        self.template_dir = template_dir
        self.original_dir = original_dir
        self.fake_dir = fake_dir

        self.transform = transform
        self.max_printed = (
            max_printed
            if max_printed is not None
            else max(
                [
                    len(list(os.listdir(os.path.join(directory, t))))
                    for t in self.template_ids
                    for directory in [self.original_dir, self.fake_dir]
                ]
            )
        )
        self.samples = {i: self._load_sample(i) for i in range(len(self))}

    def __len__(self):
        return len(self.template_ids)

    def __getitem__(self, index):
        return self.samples[index]

    def _load_img(self, path):
        if not os.path.isfile(path):
            return None

        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE).astype(np.float32)
        img = np.expand_dims(img, 2)
        return img

    def _load_sample(self, idx):
        # Getting paths to images
        template_nr = self.template_ids[idx]
        t_path = os.path.join(self.template_dir, template_nr + ".tiff")
        o_paths = [
            os.path.join(self.original_dir, template_nr, fn)
            for fn in os.listdir(os.path.join(self.original_dir, template_nr))
        ]
        f_paths = [
            os.path.join(self.fake_dir, template_nr, fn)
            for fn in os.listdir(os.path.join(self.fake_dir, template_nr))
        ]

        n_originals, n_fakes = len(o_paths), len(f_paths)

        # Loading images
        t = self._load_img(t_path)
        originals = [
            self._load_img(p) for i, p in enumerate(o_paths) if i < self.max_printed
        ]
        fakes = [
            self._load_img(p) for i, p in enumerate(f_paths) if i < self.max_printed
        ]

        # Upscaling template if necessary
        if originals[0].shape != t.shape:
            t = cv2.resize(
                t,
                (originals[0].shape[1], originals[0].shape[0]),
                interpolation=cv2.INTER_NEAREST,
            )

        # Running transform
        if self.transform is not None:
            t = self.transform(t)
            originals = [self.transform(o) for o in originals]
            fakes = [self.transform(f) for f in fakes]

        # Appending dummy tensors if necessary
        if len(originals) < self.max_printed:
            for i in range(self.max_printed - len(originals)):
                originals.append(torch.zeros_like(originals[0]))

        if len(fakes) < self.max_printed:
            for i in range(self.max_printed - len(fakes)):
                fakes.append(torch.zeros_like(fakes[0]))

        return {
            "template": t,
            "originals": torch.stack(originals),
            "fakes": torch.stack(fakes),
            "n_originals": n_originals,
            "n_fakes": n_fakes,
            "template_number": template_nr,
            "path_template": t_path,
        }


class CDPDataset(LightningDataModule):
    def __init__(
        self,
        template_dir,
        original_dir,
        fake_dir,
        train_p=0.6,
        val_p=0.2,
        max_printed=None,
        batch_size=32,
        transform=None,
        num_workers=0,
        shuffle=True,
    ):
        super(CDPDataset, self).__init__()

        self.template_dir = os.path.join(template_dir, "rcod")
        self.original_dir = os.path.join(original_dir, "rcod")
        self.fake_dir = os.path.join(fake_dir, "rcod")

        self.train_p = train_p
        self.val_p = val_p
        self.max_printed = max_printed

        self.batch_size = batch_size
        self.transform = transform
        self.num_workers = num_workers
        self.shuffle = shuffle

    def setup(self, stage: str):
        # Filtering out ids that do not have originals and fakes
        ids = [f.split(".")[0] for f in os.listdir(self.template_dir)]
        ids = [
            id
            for id in ids
            if id in os.listdir(self.original_dir) and id in os.listdir(self.fake_dir)
        ]

        # Splitting
        if self.shuffle:
            random.shuffle(ids)

        train_ids = ids[: int(len(ids) * self.train_p)]
        val_ids = ids[
            int(len(ids) * self.train_p) : int(len(ids) * (self.train_p + self.val_p))
        ]
        test_ids = ids[int(len(ids) * (self.train_p + self.val_p)) :]

        self.train_set = CDPSubset(
            train_ids,
            self.template_dir,
            self.original_dir,
            self.fake_dir,
            self.transform,
            self.max_printed,
        )
        self.val_set = CDPSubset(
            val_ids,
            self.template_dir,
            self.original_dir,
            self.fake_dir,
            self.transform,
            self.max_printed,
        )
        self.test_set = CDPSubset(
            test_ids,
            self.template_dir,
            self.original_dir,
            self.fake_dir,
            self.transform,
            self.max_printed,
        )

    def train_dataloader(self):
        return DataLoader(
            self.train_set, batch_size=self.batch_size, num_workers=self.num_workers
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_set, batch_size=self.batch_size, num_workers=self.num_workers
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_set, batch_size=self.batch_size, num_workers=self.num_workers
        )

    def teardown(self, stage: str):
        pass
